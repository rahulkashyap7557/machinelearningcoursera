function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

CSet = [0.01; 0.03; 0.05; 0.08; 0.1; 0.3; 0.5; 0.8; 1.0; 3.0; 5.0; 8.0; 10.0; 30.0; 50.0; 80.0; 100.0];
sigmaSet = [0.01; 0.03; 0.05; 0.08; 0.1; 0.3; 0.5; 0.8; 1.0; 3.0; 5.0; 8.0; 10.0];

error = zeros(length(CSet), length(sigmaSet));

for ii = 1:length(CSet)
    C_int = CSet(ii);
    for jj = 1:length(sigmaSet)
        sigma_int = sigmaSet(jj);
        model= svmTrain(X, y, C_int, @(x1, x2) gaussianKernel(x1, x2, sigma_int));
        visualizeBoundary(X, y, model);
        predictions = svmPredict(model, Xval);
        error(ii, jj) = mean(double(predictions ~= yval));
        
    end
end

[m, ind] = min(error(:));
[indrow, indcol] = ind2sub(size(error), ind);

C = CSet(indrow);
sigma = sigmaSet(indcol);







% =========================================================================

end
