# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Contains all the assignments for the Machine Learning course on Coursera


### How do I get set up? ###

Simply download the folder for each assignment and follow instructions in the pdf. You will need MATLAB or Octave to run these files.

### Who do I talk to? ###

Rahul Kashyap
Email me at rahulkashyap7557@gmail.com
